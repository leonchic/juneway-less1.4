FROM debian:9

RUN apt-get update && apt-get install -y wget make gcc libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.20.1.tar.gz && tar xvfz nginx-1.20.1.tar.gz && cd nginx-1.20.1 && ./configure && make && make install

CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]